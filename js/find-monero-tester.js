

function Tester_init(){
	ElastoHeader();

	$Browser = detectLang();
	FMLoc = ($Browser['loc']) ? $Browser['loc'] : 'US';
	FMLng = ($Browser['lng']) ? $Browser['lng'] : 'en';

	load_LanguageLib(FMLng, 'ui', function(){
		load_Country(FMLoc, FMLng, function(){
			LocLngFld(FMLoc, 'SetLocale', FMLng, 'SetLang', 'n');
			Tests_load();
		});
	});
}

function Tests_load(){
	if(FMLoc && FMLng){
		Floc = FMLoc;
		Flng = FMLng;

		console.log($LOC[FMLoc]);

		var now = getNow(),
			ins = '';

		getElem('TestStage').innerHTML = '<div id="Test_Address" class="shim20"></div>'+
			'<div class="hbar"></div>'+
			'<div id="Test_DateTime" class="shim20">'+
				'<div class="Col2">'+
					'<p>'+prepDate(now, 'dte')+'</p>'+
					'<p>'+prepDate(now, 'dte_long')+'</p>'+
					'<p>'+prepDate(now, 'tme')+'</p>'+
					'<div id="Tests_Days" class="shimtop20"></div>'+
				'</div>'+
				'<div class="Col2">'+
					'<div id="Tests_Months" class="shimtop10"></div>'+
				'</div>'+
			'</div>'+
			'<div class="hbar"></div>'+
			'<div class="shim20">'+
				'<div id="Test_Currency" class="Col2">'+
					'<p>'+prepNUM('12345678')+'</p>'+
					'<p>'+prepNUM('123545.678')+'</p>'+
					'<p>'+prepNUM('-12345678')+'</p>'+
					'<p>'+prepPERC('1234')+'</p>'+
					'<p>'+prepPERC('98.764')+'</p>'+
				'</div>'+
				'<div id="Test_Numbers" class="Col2">'+
					'<p>'+$LOC[FMLoc]['json']['cur']['iso']+' ('+$LOC[FMLoc]['json']['cur']['sym']+')</p>'+
					'<p>'+prepCUR('123456789', $LOC[FMLoc]['json']['cur']['sym'])+'</p>'+
					'<p>'+prepCUR('1234567.89', $LOC[FMLoc]['json']['cur']['sym'])+'</p>'+
				'</div>'+
			'</div>';

		ins = '<table class="TDPadS">'+
			'<tr>'+
				'<td width="10%" class="center">ISO</td>'+
				'<td width="55%">Name</td>'+
				'<td width="35%">Abbreviation</td>'+
			'</tr>';

		for(var d = 1; d <= 7; d++){
			ins += '<tr>'+
				'<td class="center">'+d+'</td>'+
				'<td>'+LocalizeDayMonth(d, 'd', FMLng, 'n')+'</td>'+
				'<td>'+LocalizeDayMonth(d, 'd', FMLng, 'y')+'</td>'+
			'</tr>';
		}
		ins += '</table>';
		getElem('Tests_Days').innerHTML = ins;

		ins = '<table class="TDPadS">'+
			'<tr>'+
				'<td width="10%" class="center">ISO</td>'+
				'<td width="55%">Name</td>'+
				'<td width="35%">Abbreviation</td>'+
			'</tr>';

		for(var m = 1; m <= 12; m++){
			ins += '<tr>'+
				'<td class="center">'+m+'</td>'+
				'<td>'+LocalizeDayMonth(m, 'm', FMLng, 'n')+'</td>'+
				'<td>'+LocalizeDayMonth(m, 'm', FMLng, 'y')+'</td>'+
			'</tr>';
		}
		ins += '</table>';
		getElem('Tests_Months').innerHTML = ins;

		Address_init('FMTest', FMLoc, FMLng, 'Test_Address', function(){
			//done
		});
	}else{
		getElem('TestStage').innerHTML = '';
	}
}

document.addEventListener('change', f => {
	var e = f.target,
		id = e.getAttribute('id'),
		val = e.value;

	if(id === 'SetLocale'){
		FMLoc = val;
		load_Country(FMLoc, FMLng, function(){
			Tests_load();
		});
		
	}else if(id === 'SetLang'){
		FMLng = val;
		load_Country(FMLoc, FMLng, function(){
			Tests_load();
		});
	}
});
