var FMResizeTimeout,
	CHov = '',
	FMLng = 'en',
	FMLoc = '',
	MapPage = 'n',
	$Browser = detectLang(),
	$Cache = {},
	$PGE_Country = {};

$ICO['loading'] = '<div class="spin">'+$ICO['spoke']+'</div>';
$ICO['close'] = '<div class="rot45">'+$ICO['plus']+'</div>';
$ICO['puzzle'] = '<svg viewBox="0 0 99 99"><g transform="translate(0 -270.8)"><path d="M77.5 278.8l-10.7 10.8c9.3 0 11.6 9 7 13.8-6.5 5-13.3.8-13.6-7.3L49.5 307 60 317.5c-9.7 1-11.2 8.8-7 13.8 5.6 5 13.2 2.3 14-6.8L77.5 335l16.3-16.4a16 16 0 00-.7-24.3zm-37.6 1.3c-4.1.2-7.4 4-8 9.3l-8.1-8.2a3.3 3.3 0 00-4.7 0L6 294.4c-7.8 7-7.7 16.7-1.4 23.7l15.1 15.2 7.4-7.4a10.5 10.5 0 01-3.6-17.2c5.4-5 14.1-3.4 16.9 3.9l7.3-7.4-8.8-8.8c10.5-1.4 11.2-9.6 7-14-2-1.6-4.1-2.3-6-2.2zm-9.7 28.1c-1.9.1-3.7.9-5 2.3-4.7 5.9-2.3 13 6.8 14L21.5 335l25.7 25.8a3.3 3.3 0 004.6 0l24-24-7.4-7.4a10.4 10.4 0 01-17.1 3.7c-5.3-6-3-14.4 3.8-17l-7.4-7.4-8.7 8.8c-.4-6.4-4.8-9.4-8.8-9.3z" /></g></svg>';
$ICO['blankpin'] = '<svg viewBox="0 0 99 99"><path d="M49.5 0a34.5 34.5 0 00-28 54.5l28 44.5 28-44.5A34.5 34.5 0 0049.5 0z"/></svg>';
$ICO['fmmap'] = '<svg viewBox="0 0 99 99"><path d="M30 3L2 12v85l28-10zm32 4l2 1V7zM36 8v52l28 22V17zm16 70zm11 17v0zM97 2l-28 9v86l28-9z" /></svg>';
$ICO['fmdir'] = '<svg viewBox="0 0 99 99"><path d="M10 69h37v28H10zm0-33h79v28H10zm0-34h79v28H10z" /></svg>';

function SpokeInitialized(){
	
	if($PGE === '404' || ($PGE['idx1'] && $PGE['idx1'] === 'MapPage')) MapPage = 'y';
	
	load_Countries(FMLng, function(){
		load_LanguageLib('', 'sys', function(){
			var path = window.location.pathname,
				$p = path.split('/');

			if($PGE === '404'){
				if(path && $p[1]){
					if((/^[a-z]{2}$/.test($p[1])) == true){
						FMLng = $p[1];
					}
				}
			}else if($PGE['xid'] && $PGE['filename'] !== 'tester'){
				FMLng = $PGE['lng'];
			}else if($Browser['lng'] && $Browser['lng'] in $LNG['sys']){
				FMLng = $Browser['lng'];
			}

			load_LanguageLib(FMLng, 'app', function(){
				if($PGE === '404'){
					if(path){
						var cty = ($p[2]) ? $p[2] : $p[1];

						cty = cty.replace('.html', '');
						cty = cty.replace(/-/g, ' ');
						cty = cty.replace(/%27/g, '’');

						for(var c in $LOC){
							if(cty === $LOC[c]['n'] || ($LOC[c][FMLng] && $LOC[c][FMLng] === cty)){
								FMLoc = c;
								break;
							}
						}
					}
					if(FMLoc){
						$PGE = {
							'xid':'C895f5b57c978173',
							'loc':FMLoc,
							'lng':FMLng,
							'pge_head':{"dna":"00010010"},
							'pge_sections':[{"id":"svgMap","class":"","html":"\n\n"}]
						};
					}
				}

				if(MapPage === 'y'){
					init_Map('main', 'full', FMLoc);
				}else if($PGE['xid'] === 'C895f5b57c978173'){
					load_Page('dyn', function(){
						init_Map('main', 'full', FMLoc);
					});
				}else{
					init_Page();
				}
			});
		});
	});
}

function init_Page(){
	FMHeader('DirHdr');
	FMFooter('FootNav');
	if($PGE['idx0'] && $PGE['idx0'] === 'Privacy'){
		load_Sections('FMPage', $PGE);
	}else if($PGE['idx0'] && $PGE['idx0'] === 'Merchants'){
		MerchantDir();
	}else if($PGE['xid'].charAt(0) === 'L'){
		MerchantDir($PGE['xid']);
	}else if($PGE['idx0'] && $PGE['idx0'] === 'Contribute'){
		ContributeLayout('');
	}else if($PGE['idx0'] && $PGE['idx0'] === 'SiteMap'){
		init_SiteMap($PGE['lng']);
	}else if($PGE['filename'] && $PGE['filename'] === 'tester'){
		load_Scripts(['js/apps/find-monero-tester'], function(){
			Tester_init();
		});
	}
	ElastoHeader();
}

function init_Map(tar, mde, loc, $d){
	var m = getElem(tar),
		ins = '<div id="FMMap" class="svgMap"></div>';

	if(mde === 'full'){
		ins += '<div id="MapHdr"></div>'+
		'<div id="CountryTile" class="Tile C0bk_grd C2br txt_copytext C3 Shadow hide"></div>'+
		'<div id="DetailTile" class="Tile C0bk_grd C2br txt_copytext C3 Shadow hide">'+
			'<div class="TitleGroup">'+
				'<div id="DetailTitle" class="TileTitle C2br C0bk_grd">'+
					'<div id="DetailTitleText" class="TileTitleInner ElastoHeader C7"></div>'+
					'<div id="DetailClose" class="TileClose Btn16 C1fl C2fl_hov"></div>'+
				'</div>'+
			'</div>'+
			'<div id="DetailBody" class="TileInner"></div>'+
		'</div>';
	}
			
	m.classList.add('mainmap');
	m.innerHTML = ins;

	OpenModal('40%','15%');
	RemoveDiv('ModalClose');
	getElem('ModalStage').innerHTML = '<div id="NotDoneYet">Coming Soon</div>';

	if(mde === 'full') FMHeader('MapHdr');

	load_Scripts(['js/lib/svgMap'], function(){
		svgMap_init('FMMap', FMLng, mde, function(){
			if(mde === 'full'){
				getElem('svgMapInner').innerHTML += '<div id="MapFooter" class="C7"></div>';
				FMFooter('MapFooter');
			}
			if(loc){
				CountryOpen(loc);
			}else if($Browser['loc'] && $Browser['loc'] in $LOC){
				MapZoom($LOC[$Browser['loc']]['z']);
				MapPan($Browser['loc'], 0);
			}
			if($d){
				MapAddPin('', $d);
			}
		});
	});
}

function init_SiteMap(lng){
	if(!lng) lng = FMLng;
	var title = ($PGE['title']) ? $PGE['title'] : 'Sitemap',
		desc = ($PGE['caption']) ? $PGE['caption'] : '',
		$F = $LNG['app'][FMLng]['nav'],
		i = 0,
		ins = '<div class="LR80 txt_copytext">'+
			'<div class="ElastoHeader C7 shimtop10">'+title+'</div>'+
			'<p>'+desc+'</p>'+
			'<div class="hbar shim10"></div>'+
			'<div class="shim10 center">';

		for(var k in $F){
			if(k !== 'sitemap'){
				if(i > 0) ins += ' &nbsp; &nbsp; ';
				ins += '<a href="'+$ENV['host']+$F[k]['url']+'">'+$F[k]['lbl']+'</a>';
			}
			i++;
		}

	ins += '</div>'+
		'<div class="hbar shim10"></div>'+
		'<div id="SiteMapLanguages" class="shim10"></div>'+
		'<div class="hbar shim10"></div>'+
		'<div id="SiteMapCountries"></div>'+
	'</div>';

	getElem('SiteMap').innerHTML = ins;
	ElastoHeader();

	//var ins = '';
	//for(var c in $LOC){
	//	var nme = CountryName(c),
	//		fle = CountryFileName(nme, FMLng);

	//	ins += '<div class="Col3 center shim5 LR98"><a href="'+$ENV['host']+fle+'">'+nme+'</a></div>';
	//}
	//getElem('SiteMapCountries').innerHTML = ins;

	db('query_Content', 'Content', {'idx0':'SiteMap', 'lim':150}, function(d){
		if(d && d['msg'] === 'OK' && d['data']){
			var ins = '';
			for(var k in d['data']){
				var $lbl = $LNG['sys'][d['data'][k]['lng']],
					lbl = ($lbl['l']) ? $lbl['l'] : $lbl['i'];

				ins += '<div class="Col4 center shim5 LR98"><a href="'+$ENV['host']+d['data'][k]['filename']+'.html">'+lbl+'</a></div>';
			}
			getElem('SiteMapLanguages').innerHTML = ins;
		}
	});
}

function CountryHover(v){
	if(CHov == v){
		//already active
	}else{
		CountryUnhover();
		var m = getElem(v);
			s = getElem('Search-'+v);

		CHov = v;

		if(MapTooltip && $LOC[v]){
			MapTooltip.innerHTML = CountryName(v);
			MapTooltip.classList.remove('hide');
		}

		if(m){
			var c = m.getAttribute('class');
			if(c.includes('svgMap_LandHover') || c.includes('svgMap_LandActive')){
				//already active
			}else{
				setAttr(m, {"class":"svgMap_Land svgMap_LandHover"});
			}
		}
		if(s) s.classList.add('SearchResulthover');
	}
}

function CountryUnhover(){
	CHov = '';
	if(typeof MapTooltip != 'undefined' && MapTooltip) MapTooltip.classList.add('hide');

	var h = document.querySelectorAll('.svgMap_LandHover'),
		s = document.querySelectorAll('.SearchResulthover'),
		hnum = h.length,
		snum = s.length,
		i = 0;

	for(i = 0; i < hnum; i++){
		setAttr(h[i], {"class":"svgMap_Land"});
	}
	for(i = 0; i < snum; i++){
		s[i].classList.remove('SearchResulthover');
	}
}

function CountryName(v){
	var r = '';
	if(v && $LOC[v]){
		r = (FMLng !== 'en' && $LOC[v][FMLng]) ? $LOC[v][FMLng] : $LOC[v]['n'];
	}
	return r;
}
function CountryFileName(nme, lng){
	var r = '';
	if(lng && lng !== 'en'){
		r += lng+'/';
	}
	r += CreateFilename(nme);
	r += '.html';

	return r;
}

function CountryOpen(loc){
	var m = getElem('CountryTile');
	if(m && m.childNodes.length > 0 && loc === FMLoc){
		//already populated
		m.classList.remove('hide');
	}else if(m){
		CountryClose();
		m.classList.remove('hide');
		
		FMLoc = loc;

		var lbl = CountryName(loc),
			fle = CountryFileName(lbl, FMLng),
			url = $ENV['host']+fle,
			ins = '<div class="TitleGroup">'+
				'<div id="CountryTitle" class="TileTitle C2br C0bk_grd">'+
					'<div id="CountryTitleText" class="TileTitleInner ElastoHeader C7">'+lbl+'</div>'+
					'<div id="CountryClose" class="TileClose Btn16 C1fl C2fl_hov">'+$ICO['close']+'</div>'+
				'</div>'+
			'</div>'+
			'<div class="TileInner">'+
				'<div id="CountryMenu"></div>'+
				LittleList($ICO['link'], DisplayURL(url), '', '', url)+
			'</div>';

		m.innerHTML = ins;
		ElastoHeader();
	}

	FMMenu_init('CountryMenu', $LNG['app'][FMLng]['menu']);
	setAttr(getElem(loc), {"class":"svgMap_Land svgMap_LandActive"});
	MapZoom($LOC[loc]['z']);
	MapPan(loc);

	if(m){
		//FMMenu_open(getElem('gallery_btn'));
		MapHiRes(loc);
	
		if(VPortW > 700){
			//MapPan('L', (m.offsetWidth / 2));
		}
		//MapPan('D', (m.offsetHeight / 2));
	}
}

function CountryClose(){
	FMLoc = '';
	var m = getElem('CountryTile'),
		h = document.querySelectorAll('.svgMap_LandActive'),
		hnum = h.length;

	for(var i = 0; i < hnum; i++){
		setAttr(h[i], {"class":"svgMap_Land"});
	}

	m.classList.add('hide');
	EmptyDiv(m);
	EmptyDiv('PointGroup');
}

function DetailOpen(){
	var e = getElem('DetailTile');
	e.classList.remove('hide');
	return e;
}
function DetailClose(){
	getElem('DetailTile').classList.add('hide');
}

function WhereToBuyWidget(tar, loc){
	var e = getElem(tar),
		shimtop = '',
		ins = '';

	if(e){
		load_Scripts(['js/lib/Forms'], function(){
			if(tar === 'MerchantResults'){
				//is the standalone page version
				shimtop = 'shimtop10';
				ins += '<p class="center shimtop30">'+$PGE['caption']+'</p>'+
					'<div id="Widget-country" class="ComboBox FMFld Widgette shimtop20"></div>'+
					'<div class="hbar shim10"></div>';
			}
			ins += '<div id="Widget1" class="'+shimtop+'">'+
				'<div id="Widget-kyc" class="BtnSubmit Widgette">'+$LNG['app'][FMLng]['buy']['kyc']+'</div>'+
				'<div class="WidgetteOr C2">'+$LNG['app'][FMLng]['or']+'</div>'+
				'<div id="Widget-nonkyc" class="BtnSubmit Widgette">'+$LNG['app'][FMLng]['buy']['p2p']+'</div>'+
			'</div>'+
			'<div class="hbar shim10"></div>'+
			'<div id="Widget2" class="'+shimtop+'">'+
				'<div id="Widget-fiat" class="BtnSubmit Widgette">'+$LNG['app'][FMLng]['buy']['fiat']+'</div>'+
				'<div class="WidgetteOr C2">'+$LNG['app'][FMLng]['or']+'</div>'+
				'<div id="Widget-btc" class="BtnSubmit Widgette">'+$LNG['app'][FMLng]['buy']['crypto']+'</div>'+
			'</div>'+
			'<div class="hbar shim10"></div>'+
			'<div id="Widget3" class="'+shimtop+'"><div class="C2 center shimtop10">['+$LNG['app'][FMLng]['buy']['prompt']+']</div></div>'+
			'<div class="LR95 shimtop5"><div class="hbar"></div></div><div class="txt_smalltext shim20 LR85 center"><i>'+$LNG['app'][FMLng]['disclaim']['exchange']+'</i></div>';

			e.innerHTML = ins;

			if(tar === 'MerchantResults'){
				load_Countries($ENV['lng']['lng'], function(){
					$CTY = {};
					for(var k in $LOC){
						$CTY[k] = {'lbl':$LOC[k]['n']};
						if($LOC[k]['a']) $CTY[k]['alt'] = $LOC[k]['a'];
					}
					ComboBox('Widget-country', loc, 'select', $CTY);
					getElem('Widget-country_fld').focus();
				});
			}
		});
	}
}

function WidgetUpdate(id){
	id = id.replace('Widget-', '');
	var w = '',
		dim = '',
		val = id;

	if(id === 'kyc' || id === 'nonkyc'){
		w = 'Widget1';
		dim = (id === 'kyc') ? 'nonkyc' : 'kyc';
	}else if(id === 'fiat' || id === 'btc'){
		w = 'Widget2';
		dim = (id === 'fiat') ? 'btc' : 'fiat';
	}

	setAttr(getElem(w), {"data-val":val});
	getElem('Widget-'+id).classList.add('WidgetteOn');
	if(dim) getElem('Widget-'+dim).classList.remove('WidgetteOn');

	WhereToBuy_Results();
}

function WhereToBuy_Results(){
	var e = getElem('Widget3'),
		l = getElem('Widget-country'),
		loc = (l) ? l.getAttribute('data-val') : FMLoc,
		mde = getElem('Widget1').getAttribute('data-val'),
		cur = getElem('Widget2').getAttribute('data-val'),
		ins = '';

	if(loc && mde && cur){
		e.innerHTML = '<div class="shim30 center"><div class="Ico24 C2fl o6">'+$ICO['loading']+'</div></div>';
		console.log($LOC);
		load_Country(loc, FMLng, function(){
			load_JSON($ENV['cdn']+'json/lib/Countries/SpokeCurrencies'+$ENV['min']+'.json'+$ENV['cache'], function(c){
				$c = JSON.parse(c);
				db('query_Content', 'Locations', {'idx2':'exchange-'+mde, 'lim':100}, function(d){
					if(d && isObj(d['data']) > 0){
						$Cache = d['data'];
						var $R = {},
							i = 0,
							fiatcur = $LOC[loc]['json']['cur']['iso'];

						for(var k in d['data']){
							var v = d['data'][k],
								block = 'n';
			
							if(v['flex']){
								if(v['flex']['allow'] === 'all'){
									if(v['flex']['block']){
										var b = FlexFind(v['flex']['block'], loc);
										if(b === 'y') block = 'y';
									}
								}else if(v['flex']['allow']){
									block = 'y';

									var f = FlexFind(v['flex']['allow'], loc);
									if(f === 'y') block = 'n';
								}
								if(block === 'n'){
									$R[i] = d['data'][k];
									i++;
								}
							}
						}
						if(cur === 'fiat'){
							var $R1 = {},
								r1i = 0,
								$R2 = {},
								r2i = 0;

							for(var r in $R){
								var curmatch = FlexFind($R[r]['flex']['currency'], fiatcur);
								if(curmatch === 'y' || $R[r]['flex']['currency'] === 'all'){
									$R1[r1i] = $R[r];
									r1i++;
								}else if($R[r]['flex']['currency']){
									$R2[r2i] = $R[r];
									r2i++;
								}
							}
							
							var fiatlbl = ($c[fiatcur] && $c[fiatcur]['lbl']) ? $c[fiatcur]['lbl']+' ('+fiatcur+')' : fiatcur;

							ins += '<div class="txt_tiny">'+fiatlbl+'</div><div class="hbar shim5"></div>';
							if(r1i > 0){
								for(var r in $R1){
									ins += RowResult('OpenMerchant', k, '', $R1[r]);
								}
							}else{
								ins += '<div class="shim20 center">'+$LNG['ui'][FMLng]['noresults']+'</div>';
							}
							if(r2i > 0){
								ins += '<div class="hbar shim5"></div>'+
									'<div class="txt_tiny shimtop10">'+$LNG['app'][FMLng]['buy']['other']+'</div>'+
									'<div class="hbar shim5"></div>';

								for(var r in $R2){
									ins += RowResult('OpenMerchant', k, '', $R2[r]);
								}
							}
						}else{
							for(var r in $R){
								ins += RowResult('OpenMerchant', k, '', $R[r]);
							}
						}
					}else{
						ins = '<div class="shim30 center">'+$LNG['ui'][FMLng]['noresults']+'</div>';
					}
					e.innerHTML = ins;
				});
			});
		});
	}
}

function MerchantDir(xid){
	var e = getElem('main').getElementsByTagName('section')[0],
		ins = '';

	ins += '<div id="DirTitleGroup" class="LR90">'+
		'<div id="DirTitle"></div>'+
		'<div class="hbar o6"></div>'+
		'<div id="DirBCT"><div id="DirBCTInner" class="txt_smalltext C2"></div></div>'+
	'</div>'+
	'<div id="DirResults" class="LR85 shimtop10">'+
		'<div id="DirCategories" class="hide"></div>'+
		'<div id="MerchantResults" class="shimtop10"><div class="shim30 center"><div class="Ico24 C2fl o6">'+$ICO['loading']+'</div></div></div>'+
	'</div>'+
	'<div id="DetailTile" class="Tile C0bk_grd C2br C3 Shadow hide">'+
		'<div class="TitleGroup">'+
			'<div id="DetailTitle" class="TileTitle C2br C0bk_grd">'+
				'<div id="DetailTitleText" class="TileTitleInner ElastoHeader C7"></div>'+
				'<div id="DetailClose" class="TileClose Btn16 C1fl C2fl_hov"></div>'+
			'</div>'+
		'</div>'+
		'<div id="DetailBody" class="TileInner"></div>'+
	'</div>';

	e.classList.add('txt_copytext');
	e.innerHTML = ins;

	if(xid){
		MerchantPage('DirResults', '');
	}else if($PGE['idx2'] === 'buy-monero'){
		WhereToBuyWidget('MerchantResults', $PGE['loc']);
	}else{
		var $C,
			$R = {'lng':FMLng+':en', 'lim':10, 'odir':'D', 'paginate':'y'};

		if($PGE['idx1'] === 'Category' && $PGE['folder']){
			$C = {'idx1':'SubCategory', 'fld':$PGE['folder'], 'typ':'p'};
			$R['idx0'] = $PGE['idx2'];
		}else if($PGE['idx1'] === 'SubCategory'){
			$R['idx1'] = $PGE['idx2'];
		}else{
			$C = {'idx1':'Category', 'typ':'f'};
		}

		MerchantCategories($C);
		MerchantList('MerchantResults', $R, 15, function(d){
			
		});
	}

	if(xid || $PGE['idx1'] === 'Category' || $PGE['idx1'] === 'SubCategory'){
		var t = getElem('DirTitle');
		t.innerHTML = '<div class="ElastoHeader C7">'+$PGE['title']+'</div>';
		ElastoHeader(t);
		FMBreadcrumb();
	}else{
		getElem('DirTitleGroup').classList.add('hide');
	}
}

function DirFirstRow(){
	return '<div id="DirFirstRow" class="right txt_smalltext shimtop5">'+
		'<div id="MerchantPagination" data-fnc="" data-pge="1"></div>'+
		'<div class="hbar o8"></div>'+
	'</div>';
}

function MerchantList(tar, $Q, lim, callback){
	db('query_Content', 'Locations', $Q, function(d){
		var ins = '',
			e = getElem(tar);
		
		if(d && isObj(d['data']) > 0){
			ins += DirFirstRow();

			$Cache = d['data'];
			var mappin = 0;
			for(var k in d['data']){
				var $d = d['data'][k],
					tmppin = '';

				if(MapPage === 'y' && ($d['latitude'] > 0 || $d['latitude'] < 0) && ($d['longitude'] > 0 || $d['longitude'] < 0)){
					mappin++;
					tmppin = mappin;
				}
				ins += RowResult('OpenMerchant', k, tmppin, $d);
			}
			
			ins += RowAdd();
			ins += '<div class="txt_smalltext shim20 center"><i>'+$LNG['app'][FMLng]['disclaim']['merchant']+'</i></div>';
		}else{
			ins = '<div class="shim30 center">'+$LNG['ui'][FMLng]['noresults']+'</div>';
		}
		if(e && ins){
			e.innerHTML = ins;
			Sizer(tar);
			Pagination_init('MerchantPagination', '1', lim, d['tot']);
		}
		callback(d);
	});
}

function FMSearch(mde, val){
	var e = (mde === 'suggest') ? getElem('HdrSearch_autoc') : getElem('SearchResultsList'),
		btn = getElem('HdrSearch_btn'),
		$Q = {'lng':FMLng},
		srchq = (mde === 'suggest') ? 'autoc' : 'srch',
		ins = '';

	if(val){
		$Q[srchq] = val;
		btn.innerHTML = $ICO['loading'];
		if(mde !== 'suggest') OpenFMSearch();

		for(var k in $LOC){
			var match = 'n';
			if(val.toUpperCase() === k){
				match = 'y';
			}else{
				var lbl = $LOC[k]['n'].toLowerCase(),
					mvl = val.toLowerCase();

				if(mvl === lbl){
					match = 'y';
				}else if(lbl.indexOf(' ') > -1){
					var $l = lbl.split(' '),
						llen = $l.length;
		
					for(var i = 0; i < llen; i++){
						if($l[i].lastIndexOf(mvl, 0) === 0){
							match = 'y';
							break;
						}
					}
				}else if(lbl.lastIndexOf(mvl, 0) === 0){
					match = 'y';
				}else if($LOC[k]['a']){
					var alen = $LOC[k]['a'].length;
					for(var i = 0; i < alen; i++){
						var alt = $LOC[k]['a'][i].toLowerCase();
						if(mvl === alt){
							match = 'y';
							break;
						}else if(alt.lastIndexOf(mvl, 0) === 0){
							match = 'y';
							break;
						}
					}
				}
			}
			if(match === 'y') ins += '<div class="AutoCompleteCountry LR98" data-loc="'+k+'">'+$LOC[k]['n']+'</div>';
		}
		if(ins) ins += '<div class="hbar shim5 LR98"></div>';
		
		db('query_Content', 'Locations', $Q, function(d){
			if(mde !== 'suggest') ins += DirFirstRow();

			if(d && isObj(d['data']) > 0){
				if(mde !== 'suggest') $Cache = d['data'];
				for(var k in d['data']){
					var v = d['data'][k];
					if(mde === 'suggest'){
						ins += '<div class="AutoCompleteResult LR98" data-word="'+k+'">'+k+' <span class="o3">('+v+')</span></div>';
					}else{
						ins += RowResult('OpenMerchant', k, '', v);
					}
				}
			}else{
				if(mde === 'suggest'){
					ins = '';
					e.classList.add('hide');
				}else{
					ins = '<div class="center shimtop50">'+$LNG['ui'][FMLng]['noresults']+'</div>';
				}
			}
			if(mde !== 'suggest') ins += RowAdd();
			e.innerHTML = ins;
			btn.innerHTML = $ICO['magnify'];
		});
	}else{
		if(mde !== 'suggest'){
			CloseFMSearch();
		}
	}
}

function OpenFMSearch(){
	getElem('SearchResults').classList.remove('hide');

	var f = getElem('HdrSearch_fld'),
		t = getElem('SearchTitle');

	t.innerHTML = '<div class="ElastoHeader C7">“'+f.value+'”</div>';
	ElastoHeader(t);
	SpokeSearch_hideauto(f);
}

function CloseFMSearch(){
	getElem('HdrSearch_autoc').classList.add('hide');
	getElem('SearchResults').classList.add('hide');
	getElem('HdrSearch_fld').value = '';
}

function MerchantPage(tar, k){
	var $P = {},
		st = '';

	if(tar === 'DetailBody'){
		DetailOpen();
		$P = $Cache[k];

		getElem('DetailTitleText').innerHTML = $P['title'];
		getElem('DetailClose').innerHTML = $ICO['close'];
		ElastoHeader();
		st = ' shimtop10';
	}else{
		k = 0;
		$P = $PGE;
		$Cache[k] = $P;
		st = ' shimtop20';
	}

	var caption = ($P['caption']) ? $P['caption'] : '',
		loc = $P['country'],
		geo = (($P['latitude'] > 0 || $P['latitude'] < 0) && ($P['longitude'] > 0 || $P['longitude'] < 0)) ? 'y' : 'n',
		phn = '',
		rcl = (VPortW >= 700) ? ' right' : '',
		pgurl = 'https://www.findmonero.org/'+$P['folder']+'/'+$P['filename']+'.html',
		ins = '';

	ins += '<div id="DetailIntro">';

	if($P['thumb']) ins += '<div class="DetailLogo Sizer" img-dna="'+$P['thumb']+'" '+FMThumb($P['thumb'])+'></div>';

	ins += '<div class="TileCopy'+st+'">'+caption+'</div>'+
		'</div>'+
		'<div class="clear"></div>'+
		'<div class="hbar LR98 shimtop10"></div>'+
		'<div class="shimtop10"><div class="Magic500 Col4-3">';

		if($P['flex']){
			ins += '<div class="shimtop10">';
				if($P['flex']['url']){
					ins += '<div class="shim5"><a href="'+$P['flex']['url']+'" target="_blank">'+DisplayURL($P['flex']['url'])+'</a></div>';
				}
				if($P['flex']['email']){
					ins += '<div class="shim5"><a href="mailto:'+$P['flex']['email']+'">'+$P['flex']['email']+'</a></div>';
				}
				if($P['flex']['social']){
					ins += '<div class="shim5"><a href="'+$P['flex']['social']+'" target="_blank">'+DisplayURL($P['flex']['social'])+'</a></div>';
				}
				if($P['flex']['phone']){
					phn = $P['flex']['phone'];
					ins += '<div class="shim5"><span id="flexphn"></span></div>';
				}
			ins += '</div>';
		}

		if(loc){
			ins += '<div id="PrintAddress" class="shimtop20">'+
				'<div class="shim20 LR90"><div class="Ico24 C2fl o6">'+$ICO['loading']+'</div></div>'+
			'</div>';
		}

	ins += '</div><div class="Magic500 Col4"><div class="shimtop10">';

	if(geo === 'y'){
		if(MapPage !== 'y') ins += '<div id="MiniMap" data-row="'+k+'"></div>';
		ins += '<div id="MiniMapStreetBtn" class="BtnSubmit FMMapBox" data-row="'+k+'">Open Street Map</div>';
	}

	ins += '</div></div>'+
	'</div>'+
	'<div class="hbar LR98 shimtop10"></div>'+
	'<div class="shimtop10">'+
		LittleList($ICO['link'], DisplayURL(pgurl), '', '', pgurl)+
		LittleList($ICO['puzzle'], 'Error? Mistranslation? Contribute!', 'ContactNav', 'contact', '')+
	'</div>';

	getElem(tar).innerHTML = ins;
	Sizer(tar);

	if(loc){
		load_Country(loc, $P['lng'], function(){
			if(phn){
				var phncde = ($LOC[loc] && $LOC[loc]['json']) ? '+'+$LOC[loc]['json']['phn']['code']+' ' : '';
				getElem('flexphn').innerHTML = phncde+$P['flex']['phone'];
			}
			DisplayAddress($P, function(a){
				getElem('PrintAddress').innerHTML = a+'<div class="shim5"></div>';
			});
		});
	}
	if(geo === 'y') init_Map('MiniMap', 'mini', loc, $P);
}

function MerchantCategories($Q){
	var e = getElem('DirCategories'),
		ins = '<div class="shimtop20">';

	e.classList.remove('hide');
	e.innerHTML = '<div class="shim30 center"><div class="Ico24 C2fl o6">'+$ICO['loading']+'</div></div>';

	if(isObj($Q) > 0){
		$Q['lng'] = FMLng+':en';
		$Q['idx0'] = 'Merchants';
		$Q['lim'] = 99;
		$Q['ocol'] = 'title';
		db('query_Content', 'Content', $Q, function(d){
			if(d && d['data']){
				var col = (VPortW > 700) ? 3 : 2,
					i = 0,
					c = 0;
	
				for(var k in d['data']){
					var v = d['data'][k],
						dsc = (v['caption']) ? '<div class="txt_smalltext o8">'+v['caption']+'</div>' : '',
						url = $ENV['host'];
	
					if(v['folder']) url += v['folder']+'/';
					if(v['filename']) url += v['filename']+'.html';
	
					if(c === 0){
						ins += '<div class="shim10">';
					}
	
					ins += '<div class="Col'+col+' center LR95"><a href="'+url+'"><b>'+v['title']+'</b></a>'+dsc+'</div>';
	
					if(c === (col - 1)){
						ins += '</div>';
						c = 0;
					}else{
						c++;
					}				
					i++;
				}

				ins += '</div>'+
				'<div class="shimtop30"></div>';
			}
			e.innerHTML = ins;
		});
	}else{
		e.innerHTML = ins;
	}
}

function FMBreadcrumb(){
	var e = getElem('DirBCTInner'),
		cat = '',
		sub = '',
		dir = '<a href="'+$ENV['host']+'merchants/" class="Btn16 C1fl C2fl_hov floatleft">'+$ICO['home']+'</a>';

	if($PGE['xid'].charAt(0) === 'L'){
		cat = $PGE['idx0'];
		sub = $PGE['idx1'];
	}else if($PGE['idx1'] && $PGE['idx1'] === 'SubCategory' && $PGE['idx2']){
		cat = $PGE['folder'];
	}else if($PGE['idx1'] && $PGE['idx1'] === 'Category'){
		//skip
	}else{
		dir = '';
	}

	if(sub){
		db('get_Content', 'Content', {'fld':cat, 'mde':'min'}, function(d){
			if(d && d['data']){
				cat = ' / <a href="'+$ENV['host']+d['data']['folder']+'/">'+d['data']['title']+'</a>';
				db('query_Content', 'Content', {'lng':$PGE['lng'], 'typ':'p', 'idx2':sub, 'lim':1, 'mde':'min'}, function(s){
					if(s && s['data'] && s['data'][0]){
						sub = ' / <a href="'+$ENV['host']+s['data'][0]['folder']+'/'+s['data'][0]['filename']+'.html">'+s['data'][0]['title']+'</a>';
					}
					e.innerHTML = dir+cat+sub+' / '+$PGE['title'];
				});
			}
		});

	}else if(cat){
		db('get_Content', 'Content', {'fld':cat, 'mde':'min'}, function(d){
			if(d && d['data']){
				cat = ' / <a href="'+$ENV['host']+d['data']['folder']+'/">'+d['data']['title']+'</a>';
				e.innerHTML = dir+cat+' / '+$PGE['title'];
			}
		});
	}else if($PGE['idx1'] && $PGE['idx1'] === 'Category'){
		e.innerHTML = dir+' / '+$PGE['title'];
	}
}



function Gallery_data(tar, $Q, callback){
	var e = getElem(tar),
		ins = '';

	if(e){
		db('query_Content', 'Content', $Q, function(d){
			if(d && isObj(d['data']) > 0){
				$Cache = d['data'];

				for(var k in $Cache){
					if($Cache[k]['thumb']){
						var $THM = prepIMG($Cache[k]['thumb']);
						if($THM){
							ins += '<div class="Col5 GalleryShell o9_hov" data-row="'+k+'">'+
								'<div class="GalleryThumb C1br Sizer" img-dna="'+$Cache[k]['thumb']+'"></div>'+
							'</div>';
						}
					}
				}
			}

			ins += '<div class="Col5 GalleryAdd">'+
				'<div class="ContactNav Btn36 C1fl C2fl_hov" data-url="upload">'+$ICO['plus']+'</div>'+
			'</div>';

			e.innerHTML = ins;
			Sizer(e);
			callback();
		});
	}
}

function Gallery_item(k){
	OpenModal('97%','97%');

	var cbtn = getElem('ModalClose'),
		cls = 'C0bk_grd',
		clr = '',
		imgcss = '',
		ttl = ($Cache[k]['title']) ? $Cache[k]['title'] : '',
		dsc = ($Cache[k]['caption']) ? $Cache[k]['caption'] : '';

	cbtn.classList.add('FMBtn', 'C2br', 'Shadow');
	cbtn.innerHTML = $ICO['loading'];

	if($Cache[k]['thumb']){
		var $THM = prepIMG($Cache[k]['thumb']),
			url = $ENV['cdn']+$THM['min'];

		imgcss = ' style="background-image:url('+url+')" img-dna="'+$Cache[k]['thumb']+'"';
		if($THM['color']) clr = ' style="background-color:'+$THM['color']+';background-image:url('+url+')"';
		cls = 'Sizer';
	}

	getElem('ModalStage').innerHTML = '<div id="GalleryLable1" class="txt_copytext"><div class="C0bk_grd C3">'+ttl+'</div></div>'+
		'<div id="GalleryLable2" class="txt_smalltext"><div class="C0bk_grd C2">'+dsc+' <span class="C1">'+$LNG['app'][FMLng]['src']+'</span></div></div>'+
		'<div id="GalleryFrame"></div>'+
		'<div id="GalleryImage" class="'+cls+'"'+imgcss+'></div>'+
		'<div id="GalleryScreen"'+clr+'></div>';

	Sizer('Modal');
	cbtn.innerHTML = '<div class="C1fl C2fl_hov">'+$ICO['close']+'</div>';
}

function Contribute(nav){
	nav = nav || 'main';
	OpenModal('90%','95%');
	var cbtn = getElem('ModalClose');

	cbtn.classList.add('FMBtn', 'C2br', 'Shadow');
	cbtn.innerHTML = $ICO['loading'];

	db('query_Content', 'Content', {'typ':'p', 'idx0':'Contribute', 'lng':FMLng+':en', 'lim':1, 'mde':'pge'}, function(d){
		if(d && isObj(d['data']) > 0){
			load_Sections('ModalStage', d['data'][0]);
			cbtn.innerHTML = '<div class="C1fl C2fl_hov">'+$ICO['close']+'</div>';
			ContributeLayout(nav);
			ElastoHeader();
		}
	});
}

function ContributeLayout(nav){
	var ins = '',
		m = getElem('ContactBody'),
		giturl = 'https://gitlab.com/monerooutreach/findmonero.org',
		gitico = '<svg viewBox="0 0 99 99"><path d="M93.3 38.8l5.5 17.4c.5 1.5 0 3.3-1.4 4.3L49.5 96zm-87.6 0L.2 56.2c-.5 1.5 0 3.3 1.4 4.3L49.5 96z" fill="#a0771c"/><path d="M93.3 38.8H67.7l11-34.5c.6-1.7 3-1.7 3.6 0zm-87.6 0h25.6l-11-34.5c-.6-1.7-3-1.7-3.6 0zM49.5 96l18.2-57.2H31.3z" fill="#824114"/><path d="M31.3 38.8L49.5 96 5.7 38.8zm62 0L49.5 96l18.2-57.2z" fill="#a0561c"/></svg>';

	ins += '<div class="shimtop20">'+
		LittleList(gitico, DisplayURL(giturl), '', 'blank', giturl)+
		'<div id="ContributeMenu"></div>'+
	'</div>';

	m.innerHTML = ins;
	FMMenu_init('ContributeMenu', $LNG['app'][FMLng]['contact']);

	if(nav){
		var btn = getElem(nav+'_btn');
		if(btn) FMMenu_open(btn);
	}
}

function ContributeForm(nav){
	load_Scripts(['js/lib/Forms'], function(){
		load_LanguageLib(FMLng, 'ui', function(){
			var $f = {
				'btn':$LNG['ui'][FMLng]['send'],
				'btn_sending':$LNG['ui'][FMLng]['sending'],
				'fld':{},
				'mde':'contribute',
				'rsp':'Thank you. Someone will review your message and get back to you if needed.'
			};

			if(nav === 'contact'){
				$f['mde'] = 'contact';
				$f['fld'] = {
					0:{
						'email':{
							'lbl':'Email / Social Handle',
							'plh':'Optional if you need a reply...'
						},
						'message':{
							'lbl':'Message',
							'plh':'Comments, corrections or suggestions...',
							'req':'y'
						}
					}
				};
			}else if(nav === 'suggest'){
				$f['tbl'] = 'Locations';
				$f['rsp'] = 'Thank you. We’ll review your merchant suggestion shortly.';
				$f['fld'] = {
					0:{
						'title':{
							'lbl':'Company Name',
							'plh':'Joe’s Garage',
							'req':'y'
						},
						'caption':{
							'lbl':'Company Description',
							'plh':'We could jam in Joe’s Garage. His mama was screamin’ His dad was mad. We was playin’ the same old song. In the afternoon ’n sometimes we would play it all night long.'
						},
						'flex_url':{
							'lbl':'URL',
							'plh':'https://www.joesgarage.com',
							'req':'y'
						},
						'country':{
							'lbl':$LNG['ui'][FMLng]['fld']['country'],
							'def':FMLoc
						}
					}
				};
			}else if(nav === 'upload'){
				$f['btn'] = $LNG['ui'][FMLng]['upload'];
				$f['btn_sending'] = $LNG['ui'][FMLng]['uploading'];
				$f['tbl'] = 'Content';
				$f['fld'] = {
					0:{
						'upload':{
							'lbl':'Upload .jpg',
							'plh':'Select image from your hardrive',
							'req':'y'
						},
						'country':{
							'lbl':'Country',
							'plh':'Country...',
							'req':'y'
						},
						'city':{
							'lbl':'City',
							'plh':'City...',
							'req':'y'
						},
						'note':{
							'lbl':'Note/Comment',
							'plh':''
						}
					}
				};
			}
			SpokeForm(nav+'_exp', $f);
		});
	});
}

/*! UI Helpers */
function FMHeader(tar){
	if(tar === 'MapHdr'){
		var e = getElem(tar);
		e.innerHTML = $APP['head']['html'];
		getElem('HdrBar').classList.add('hide');
	}
	var btn = getElem('HdrBtn');
	if(btn) btn.innerHTML = (tar === 'DirHdr') ? $ICO['fmdir'] : $ICO['fmmap'];

	getElem('main').innerHTML += '<div id="SearchResults" class="hide C0bk_grd txt_copytext">'+
		'<div class="LR80">'+
			'<div id="SearchTitle">Searching...</div>'+
			'<div class="hbar o6"></div>'+
			'<div id="SearchBCT" class="txt_smalltext C2"><span class="CloseSearchResults C1_link">Close</span> Search Results</div>'+
			'<div id="SearchResultsList"></div>'+
		'</div>'+
	'</div>';

	SpokeSearch_init('HdrSearch', 'autoc', $LNG['app'][FMLng]['findxmr']);
}
function FMHeaderMenu(){
	var e = getElem('HdrExpand'),
		c = e.classList,
		b = getElem('HdrBtn'),
		mde = (e.closest('#MapHdr')) ? 'dir' : 'map';

	if(c.contains('hide')){
		b.innerHTML = $ICO['close'];
		c.remove('hide');
		var lbl = '<a href="'+$ENV['host']+$LNG['app'][FMLng]['nav'][mde]['url']+'">'+$LNG['app'][FMLng]['nav'][mde]['lbl']+'</a>',
			ico = (mde === 'dir') ? $ICO['fmdir'] : $ICO['fmmap'],
			clb = $LNG['app'][FMLng]['nav']['contact']['lbl'];

		e.innerHTML = '<div class="HdrExp">'+lbl+'<div class="HdrIco C2fl o5">'+ico+'</div></div>'+
			'<div class="HdrExp"><span class="ContactNav C1_link">'+clb+'</span><div class="HdrIco C2fl o5">'+$ICO['puzzle']+'</div></div>'+
			'<div class="hbar"></div>'+ 
			'<div class="HdrExpSel shimtop5"><select id="HdrLng" data-mde="app" data-abbr="n" data-typ="loose"></select><div class="HdrIco C2fl o5">'+$ICO['translate']+'</div></div>'+
			'<div class="HdrExp">'+$LNG['app'][FMLng]['app']+'<div id="AppSwitch" class="HdrIco Switch C2fl">'+$ICO['switch']+'</div></div>'+
			'<div class="HdrExp">'+$LNG['app'][FMLng]['cookie']+'<div id="CookieSwitch" class="HdrIco Switch C2fl">'+$ICO['switch']+'</div></div>';

		load_Scripts(['js/lib/Forms'], function(){
			Switch('F', 'AppSwitch');
			Switch('F', 'CookieSwitch');
			LocLngFld(FMLoc, '', FMLng, 'HdrLng', 'n');
		});
	}else{
		b.innerHTML = (mde === 'dir') ? $ICO['fmmap'] : $ICO['fmdir'];
		c.add('hide');
	}
}

function FMFooter(tar){
	var e = getElem(tar),
		$Funcs = {'contact':'ContactNav'};
		$F = $LNG['app'][FMLng]['nav'],
		i = 0,
		ins = '';

	if(e && $F){
		if(MapPage === 'y'){
			delete $F['map'];
		}else if($PGE['xid'].charAt(0) === 'L' || ($PGE['idx0'] && $PGE['idx0'] === 'Merchants')){
			delete $F['dir'];
		}
		for(var k in $F){
			if(i > 0) ins += ' &#124; ';
			var cls = ($Funcs[k]) ? $Funcs[k] : '';
			ins += FMTxtLink($ENV['host']+$F[k]['url'], cls, '', $F[k]['lbl']);
			i++;
		}
		ins += '<div class="txt_tiny C7 center shimtop10">'+$LNG['app'][FMLng]['mapfoot']+'</div>';
		e.innerHTML = ins;
	}
}

function FMResize(){
	clearTimeout(FMResizeTimeout);
	FMResizeTimeout = setTimeout(function(){
		ElastoHeader();
		//check this, not sure what it's doing
		var mm = getElem('MastMenu'),
			w = 0;

		if(mm) w = mm.clientWidth;
	}, 200);
}
function RowResult(mde, row, mappin, $d){
	var thm = '',
		title = ($d['title']) ? $d['title'] : '',
		caption = ($d['caption']) ? $d['caption'] : '',
		lnk = '',
		fld = $d['folder']+'/',
		url = $ENV['host']+fld+$d['filename']+'.html',
		thlnk = '';

	if($ENV['mode'] === 'app' || MapPage === 'y' || !$d['filename']){
		lnk = '<span class="'+mde+' C1_link" data-row="'+row+'">'+title+'</span>';
	}else{
		lnk = '<a href="'+url+'">'+title+'</a>';
		thlnk = 'Link';
	}

	var txt = lnk+' &middot; '+caption,
		FMthm = '';

	if($d['thumb']){
		FMthm = FMThumb($d['thumb']);
	}else{
		mde += ' RowThumbEmpty';
	}
	thm = '<div class="RowThumb '+mde+' '+thlnk+'" data-url="'+url+'" data-row="'+row+'"'+FMthm+'></div>';

	var ins = '<div class="Row">'+thm;
	if(mappin && mappin > 0){
		MapAddPin(mappin, $d);
		ins += '<div class="RowMapPin">'+
			'<div class="RowMapPinNum C0">'+mappin+'</div>'+
			'<div class="RowMapPinIco C1fl C2fl_hov">'+$ICO['blankpin']+'</div>'+
		'</div>';
	}
	ins += '<div class="RowText">'+txt+'</div></div>';

	return ins;
}
function RowAdd(){
	return '<div class="hbar o6"></div>'+
	'<div class="RowAdd">'+
	'<div class="RowThumb BtnSubmit ContactNav" data-url="suggest"><div class="Ico16 C1fl">'+$ICO['plus']+'</div></div>'+
	'<div class="RowText">'+
		'<span class="txt_tiny C2">'+$LNG['app'][FMLng]['contact']['suggest']+'</span>'+
	'</div></div>';
}

function FMMapBox(rowid){
	OpenModal('97%','97%');

	var $d = {},
		cbtn = getElem('ModalClose');

	cbtn.classList.add('FMBtn', 'C2br', 'Shadow');
	cbtn.innerHTML = $ICO['loading'];

	getElem('ModalStage').innerHTML = '<div id="LocationEdit_Map" class="fullW fullH"><div class="StageLoader Ico48 C3fl o5">'+$ICO['loading']+'</div></div>';

	if($Cache && $Cache[rowid]){
		$d = $Cache[rowid];

		if($d && $d['latitude'] && $d['longitude']){
			load_CSS('lib/MapBox-v1-12-0');
			load_Scripts(['js/lib/Maps', 'js/lib/MapBox-v1-12-0'], function(){

				cbtn.innerHTML = '<div class="C1fl C2fl_hov">'+$ICO['close']+'</div>';

				mapboxgl.accessToken = 'pk.eyJ1IjoibW9uZXJvb3V0cmVhY2giLCJhIjoiY2tpNHAzMXBxMGVkcTJ3bnd3MnpoZ2tyZCJ9.VFcW2gIO-YJFHHyPBoTzpw';
				
				var lat = $d['latitude'],
					lng = $d['longitude'],
					zoom = 10,
					map = new mapboxgl.Map({
						container: 'LocationEdit_Map',
						style: 'mapbox://styles/mapbox/streets-v11',
						center: [lng, lat],
						zoom: zoom
					});
				
				map.addControl(new mapboxgl.NavigationControl());
				
				var marker = new mapboxgl.Marker({
					draggable: true
				}).setLngLat([lng, lat]).addTo(map);
					
				function onDragEnd(){
					//var lngLat = marker.getLngLat();
					//setVal('Loc_latitude', lngLat.lat);
					//setVal('Loc_longitude', lngLat.lng);

					//EditLocations_update('Loc_latitude');
					//EditLocations_update('Loc_longitude');
				}
					
				marker.on('dragend', onDragEnd);
			});
		}
	}
}

function FMThumb(v){
	var thm = '';
	if(v){
		if(v.indexOf('|') > -1){
			var $THM = prepIMG(v),
				brd = ($THM['typ'] === 'png') ? 'border:0;' : '',
				clr = ($THM['typ'] === 'jpg' && $THM['color']) ? 'background-color:#'+$THM['color']+';' : '';

			thm = ' style="'+brd+clr+'background-image:url('+$ENV['cdn']+$THM['min']+')"';
		}else{
			thm = ' style="background-color:#fff;background-image:url('+$ENV['cdn']+'img/'+v+')"';
		}
	}
	return thm;
}
function ElastoHeader(e){
	if(e){
		e = e.querySelectorAll('.ElastoHeader');
	}else{
		e = document.getElementsByClassName('ElastoHeader');
	}
	var len = e.length,
		max = 22;

	if(VPortW > 1100){
		max = 28;
	}else if(VPortW > 900){
		max = 27;
	}else if(VPortW > 700){
		max = 26;
	}else if(VPortW > 500){
		max = 24;
	}

	for(var i = 0; i < len; i++){
		var lbl = e[i].textContent,
			f = Math.floor(e[i].parentNode.parentElement.clientWidth / lbl.length * 15) / 10;

		if(f > max) f = max;
		e[i].style.fontSize = f+'px';
		e[i].innerHTML = lbl;
	}
}
function LittleList(ico, lbl, cls, attr, url){
	return '<div class="MenuList">'+
		'<div class="MenuIco"><div class="C2fl o8">'+ico+'</div></div>'+
		'<div class="ListLbl C2br noselect">'+FMTxtLink(url, cls, attr, lbl)+'</div>'+
	'</div>';
}
function FlexFind(d, needle){
	var r = 'n';
	if(d && d !== undefined){
		if(d.indexOf(',') > 0){		//if comma delim
			var $d = d.split(','),
				len = $d.length;
	
			for(var i = 0; i < len; i++){
				if(needle === $d[i]){
					r = 'y';
					break;
				}
			}
		}else if(needle === d){		//if single value
			r = 'y';
		}
	}
	return r;
}

function FMMenu_init(tar, $d){
	var ins = '';
	for(var k in $d){
		if(k !== 'upload'){			//turn of gallery upload until the map is ready
			ins += '<div class="MenuGroup" data-mde="'+k+'">'+
				'<div id="'+k+'_btn" class="MenuBtn C1fl C2fl_hov">'+$ICO['arrow']+'</div>'+
				'<div id="'+k+'_lbl" class="MenuLbl C2br noselect">'+$d[k].replace('##C##', CountryName(FMLoc))+'</div>'+
			'</div>'+
			'<div id="'+k+'_exp" class="MenuStage hide"></div>';
		}
	}
	getElem(tar).innerHTML = ins;
}

function FMMenu_open(e){
	var p = e.parentNode,
		mde = p.getAttribute('data-mde'),
		stg = getElem(mde+'_exp');

	if(stg.classList.contains('hide')){
		var grp = p.parentNode.querySelectorAll('.MenuGroup'),
			grplen = grp.length,
			btn = getElem(mde+'_btn');
		
		for(var i = 0; i < grplen; i++){
			FMMenu_close(grp[i])
		}
		stg.classList.remove('hide');
		btn.classList.add('rot90');

		if(!stg.innerHTML.trim().length > 0){
			var fnc = 'FMMenu_'+mde;
			stg.innerHTML = '<div class="shim20 center"><div class="Ico24 C2fl o6">'+$ICO['loading']+'</div></div>';
			if(typeof window[fnc] == 'function') window[fnc]();
		}
	}else{
		FMMenu_close(p);
	}
}

function FMMenu_close(e){
	var mde = e.getAttribute('data-mde'),
		btn = getElem(mde+'_btn');

	btn.innerHTML = $ICO['arrow'];
	btn.classList.remove('rot90');
	getElem(mde+'_exp').classList.add('hide');
}

function FMMenu_buy(){
	WhereToBuyWidget('buy_exp', FMLoc);
}
function FMMenu_gallery(){
	Gallery_data('gallery_exp', {'idx0':'Monero Geographic', 'idx2':FMLoc, 'lim':18, 'ocol':'created', 'odir':'D', 'paginate':'y'}, function(){
		getElem('gallery_btn').innerHTML = $ICO['arrow'];
	});	
}
function FMMenu_merchant(){
	var lim = 6;
	if(VPortH > 900){
		lim = 10;
	}else if(VPortH > 700){
		lim = 8;
	}
	MerchantList('merchant_exp', {'country':FMLoc, 'lng':FMLng+':en', 'lim':lim, 'odir':'D', 'paginate':'y'}, lim, function(){
		getElem('merchant_btn').innerHTML = $ICO['arrow'];
	});
}
function FMMenu_contact(){
	ContributeForm('contact');
}
function FMMenu_suggest(){
	ContributeForm('suggest');
}
function FMMenu_upload(){
	ContributeForm('upload');
}
function FMMenu_donate(){
	getElem('donate_exp').innerHTML = '<div class="Magic500 Col3-2 LR98">'+
		'<p>'+$LNG['app'][FMLng]['donate']+'</p>'+
	'</div>'+
	'<div class="Magic500 Col3 LR98">'+
		'<div class="FMQR"><img src="'+$ENV['cdn']+'img/find-monero/svg/donationqr.svg" /></div>'+
	'</div>'+
	'<p><label>FindMonero Donation Address</lable><textarea>47oKHkoaQdBdFpTJNKaetUS6UsCGHVbJbGxPGaaHFQPqXSCLbqXYsBo6x7abwtfdXTeiBhtZLnYF5bRRAhYsUVb5Sd1aqiD</textarea></p>';
}

function FMTxtLink(url, cls, attr, lbl){
	var r = '',
		nw = (attr === 'blank') ? ' target="_blank"' : '',
		a = (cls && attr) ? ' data-url="'+attr+'"' : '';

	if(url){
		if(cls){
			if(MapPage === 'y' || $ENV['mode'] === 'app'){
				r += '<u class="C1_link '+cls+'"'+a+'>'+lbl+'</u>';
			}else{
				r += '<a href="'+url+'"'+nw+'>'+lbl+'</a>';
			}
		}else{
			r += '<a href="'+url+'"'+nw+'>'+lbl+'</a>';
		}
	}else if(cls){
		r += '<u class="C1_link '+cls+'"'+a+'>'+lbl+'</u>';
	}
	return r;
}

function DisplayURL(url){
	url = url.replace('https://', '');
	url = url.replace('http://', '');
	url = url.replace('www.', '');
	if(url.substr(-1) === '/') url = url.substr(0, url.length - 1);
	return url;
}

function DisplayAddress($d, callback){
	var r = '';
	if($d && $d['country'] && ($d['city'] || $d['address1'])){
		var loc = $d['country'];
		load_LanguageLib($d['lng'], 'ui', function(){
			
			var cline = '##t## ##p##',
				adr1 = $d['address1'] || '',
				adr2 = ($d['address2']) ? '<br>'+$d['address2'] : '',
				AdrHTML = adr1+adr2;
		
			if(AdrHTML != '' && loc !== 'HU') r += AdrHTML+'<br>';
		
			if($LOC[loc]['json'] && $LOC[loc]['json']['postal']){
				var $C = $LOC[loc]['json'],
					city = $d['city'] || '',
					state = '',
					state_abbr = '',
					postal = $d['postal'] || '';

				if($C['postal']['one']){
					postal = $C['postal']['one'];
				}

				if($C['postal']['cityline']) cline = $C['postal']['cityline'];

				if($d['state']){
					state = $d['state'];
					state_abbr = state;
					if(isObj($C['prov']['list']) > 0){
						var i = 0,
							st = state;

						if($C['prov']['list'][st]){
							var v = $C['prov']['list'][st];
							if(isObj(v) > 0){
								for(var l in v){
									if(i === 0){
										if(v[l]['n']) state = v[l]['n'];
										if(v[l]['a']) state_abbr = v[l]['a'];
									}
									if(l === $d['lng']){
										state = v[l]['n'];
										break;
									}
									i++;
								}
							}else if(v){
								state = v;
							}
						}
					}
				}
		
				cline = cline.replace('##t##', city);
				cline = cline.replace('##S##', state);
				cline = cline.replace('##s##', state_abbr);
				cline = cline.replace('##p##', postal);
				cline = cline.replace('_', '<br>');
				r += cline;
			}else{
				r += $d['city'];
			}
		
			if(AdrHTML != '' && loc === 'HU') r += '<br>'+AdrHTML;
			r += '<br>'+CountryName(loc);
			callback(r);
		});
	}else if($d && $d['country']){
		callback(CountryName($d['country']));
	}else if($d){
		callback('Everywhere & Nowhere');
	}
}

/*! FindMonero Events */
window.addEventListener('resize', FMResize);

document.addEventListener('keydown', f => {
	var e = f.target,
		id = e.getAttribute('id');

	if(f.keyCode === 27){
		var m = getElem('CountryTile');
		if(m && !m.classList.contains('hide')){
			CountryClose();
		}
	}
});

document.body.addEventListener('pointerup', f => {
	var e = f.target,
		id = e.getAttribute('id');

	if(id === 'HdrBtn'){
		FMHeaderMenu();
	}else if(id === 'HdrLng'){
		console.log('Lang Switch');
		console.log(e.value);
	}else if(id === 'MapModalClose'){
		FindMoneroInfoClose();
	}else if(id === 'CountryClose'){
		CountryClose();
	}else if(id === 'DetailClose'){
		DetailClose();
	}else{
		var c = e.classList;
		if(c.contains('svgMap_Land')){
			if(!c.contains('svgMap_LandActive')){
				if(typeof $Drg != 'undefined'){
					if($Drg['x'] == null) CountryOpen(id);
				}else{
					CountryOpen(id);
				}
			}
		}else if(c.contains('AutoCompleteCountry')){
			var loc = e.getAttribute('data-loc');
			if(MapPage === 'y' || $ENV['mode'] === 'app'){
				CountryOpen(loc);
				CloseFMSearch();
			}else if($LOC[loc]){
				document.location.href = $ENV['host']+CountryFileName($LOC[loc]['n'], FMLng);
			}
		}else if(c.contains('AutoCompleteResult')){
			var val = e.getAttribute('data-word');
			getElem('HdrSearch_fld').value = val;
			FMSearch('', val);
		}else if(c.contains('CloseSearchResults')){
			CloseFMSearch();
		}else if(c.contains('SearchResult')){
			CountryOpen(id.replace('Search-', ''));
		}else if(c.contains('OpenMerchant')){
			MerchantPage('DetailBody', e.getAttribute('data-row'));
		}else if(c.contains('GalleryShell')){
			Gallery_item(e.getAttribute('data-row'));
		}else if(c.contains('Widgette')){
			WidgetUpdate(id);
		}else if(c.contains('ComboBoxOption')){
			var p = e.parentNode.getAttribute('id');
			if(p === 'Widget-country_sel'){
				WidgetUpdate('Widget-country');
			}
		}else if(c.contains('FMMapBox')){
			FMMapBox(e.getAttribute('data-row'))
		}else if(c.contains('OpenExchange')){
			WhereToBuy_Q3(e.getAttribute('data-row'));
		}else if(c.contains('MenuBtn') || c.contains('MenuLbl')){
			FMMenu_open(e);
		}else if(c.contains('ContactNav')){
			Contribute(e.getAttribute('data-url'));
		}
	}
});

document.body.addEventListener('pointerover', f => {
	var e = f.target,
		c = e.classList,
		id = e.getAttribute('id');

	if(id === 'CountryTile'){
		//if($Drg['e']) DraggerRelease();
	}else{
		if(c.contains('SearchResult')){
			CountryHover(id.replace('Search-', ''));
		}else if(c.contains('svgMap_Land')){
			f.stopPropagation();
			CountryHover(id);
		}else{
			CountryUnhover();
		}
	}
});

document.body.addEventListener('pointerleave', f => {
	var e = f.target,
		id = e.getAttribute('id');

	if(id === null){
		CountryUnhover();
	}
});

//right click stuff
//document.body.addEventListener('contextmenu', function(e){
//	if($Drg['e']) DraggerRelease();
//}, false);