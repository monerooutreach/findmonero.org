FindMonero.org runs on <a href="https://gitlab.com/monerooutreach/spoke" target="_blank">Spoke</a>, Monero Outreach’s CMS/CRM. This repo contains the static resources specific to FindMonero.

To help with translations, see the files in /json/lng.

Suggestions to improve the code are welcome.

To suggest merchants and content corrections, please use the <a href="https://www.findmonero.org/contribute.html" target="_blank">Find Monero Contribute</a> form.
